WITH RAW AS (
	SELECT DISTINCT
		TRIM(LOCATION) AS LOCATION
		,TRIM(PN) AS PN
		,QTY_AVAILABLE
	FROM [omni].files.Current_Inventory
	WHERE LOCATION <> ' ' AND QTY_AVAILABLE > 0
	AND TRIM(LOCATION) NOT IN ('US TECH', 'AIRPAC', 'ADMIN', 'DRETLOH', 'ETAR', 'CYVR', 'KOSC4', 'SKYPAXXX', 'MTU - HANN', 'RJTY', 'AUSTIN-TEC', 'KVCV', 'KRIV', 'ORBI', 'PANC', 'LROP', 'UTC MIA', 'OAKB', 'OKBK', 'RKSO', 'KAEX', 'KATL/DELTA', 'KLM', 'MTU DALLAS', 'ORER', 'NEWTON', 'KGYRQ3', 'ROLLS', 'KFOE', 'KORF')
)
,CLEAN AS (
	SELECT DISTINCT
		CASE 
			WHEN TRIM(LOCATION) = 'BRAVO FAK' THEN 'FAK'
			WHEN TRIM(LOCATION) = 'ALPHA FAK' THEN 'FAK'
			WHEN (LEN(LOCATION) = 6 AND RIGHT(LOCATION,2) = 'AX' AND LEFT(LOCATION,1) = 'N') THEN 'FAK'
			ELSE LOCATION
			END AS LOCATION
		,PN
	FROM RAW 
)
,FINAL AS (
	SELECT DISTINCT
		A.LOCATION
		,A.PN
	FROM CLEAN A
	WHERE A.LOCATION = 'FAK'
)
-- INSERT INTO TRAX_CRITICALITY
SELECT *
FROM FINAL

